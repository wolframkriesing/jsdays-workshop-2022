# Repository with all Workshop Material

Here you can find:
- a setup for writing the tests

## Setup

To use this repo and just get the environment we need for the workshop up and running,
which is basically just have a test runner, you can do one of the following

1) If you have nvm on your system run `nvm use` and the latest node should be available
2) If you prefer docker, run `docker-compose run node bash` and you have an env where `node` executable is available
   in the right version
3) If you have node installed globally, make sure it is version 17 (as known at the time of writing this).

## Use

To run the tests, located in `tests` directory run `npm test`.  
To run the tests in watch mode run `npm run dev:test`.

## The Workshop

It was announced [on the javascript-days.de](https://javascript-days.de/javascript/discover-javascript-the-language-from-the-spec-and-by-writing-tests-teil-1/).

The abstract is as follows:
```
# Discover JavaScript, the Language, from the Spec and by Writing Tests

This workshop is about test-writing, and about reading and understanding the JavaScript Specification (a bit better). The combination of these two is what had led to http://jskatas.org, we will follow the trails and look at JavaScript and testing from a different angle. We will start by exploring how good we are writing tests. Next, we will define what a good test is. For the fun of it we will do a kata that teaches us one new JavaScript feature every once in a while. 

We will dive into the JavaScript (ECMAScript) Specification, try to make sense of it, at least remove the fear looking up things from time to time, to gain a deeper understanding of the language. In the end I hope that everyone will be able to pick a JavaScript feature, distill the essence of it, write it out and build code that can teach this feature, and all that correct as by the JavaScript Specification. If there is extra time and interest, we can touch on how to apply this in your daily work, where other techniques like mocking, or the test pyramid come into play. These would be things we do in a group discussion way, if desired.
```

## License

Why AGPL? So everything based on this repo is open source too. Fair deal, right? If you want to make money with it, ping me.